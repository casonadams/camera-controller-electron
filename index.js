const { app, BrowserWindow, ipcMain } = require('electron')
const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

function createWindow () {
  // Read the config file
  let config;
  try {
    // Use path.join to get the correct path to the config.yaml file
    // let localConfigPath = path.join(__dirname, 'config.yaml');
    let packagedConfigPath = path.join(process.resourcesPath, 'app', 'config.yaml');
    let localConfigPath = path.join('.', 'config.yaml');

    // Check if local config.yaml exists
    if (fs.existsSync(localConfigPath)) {
      config = yaml.load(fs.readFileSync(localConfigPath, 'utf8'));
    } else {
      // If local config.yaml does not exist, use the packaged config.yaml
      config = yaml.load(fs.readFileSync(packagedConfigPath, 'utf8'));
    }

  } catch (err) {
    console.error('Error reading config.yaml:', err)
    return;
  }

  const win = new BrowserWindow({
    width: config.window.width,
    height: config.window.height,
    backgroundColor: config.window.backgroundColor,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    }
  })

  win.loadFile('index.html')

  win.once('ready-to-show', () => {
    win.show()

    // Send the config to the renderer process
    win.webContents.send('config', config)
  })
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

