const { ipcRenderer } = require('electron')
const { exec } = require('child_process');


ipcRenderer.on('config', (event, config) => {
  console.log(config)

  const app = document.getElementById('app')
  if (!app) {
    console.error('Element with id "app" not found')
    return
  }

  if (!config.elements || !Array.isArray(config.elements)) {
    console.error('config.elements is not an array')
    return
  }

  config.elements.forEach(element => {
    if (element.type === 'BUTTON') {
      const button = document.createElement('button')
      button.innerHTML = `${element.label}`
      button.style.position = "absolute"
      button.style.border = "1px solid black"
      button.style.borderRadius = "5px"
      button.style.padding = "0px"
      button.style.fontSize = "14px"
      button.style.fontWeight = "bold"

      button.style.width = "100px"
      button.style.height = "50px"

      button.style.color = "#1d1f21"
      button.style.backgroundColor = '#808080';
      button.style.backgroundColor = "#c5c8c6"
      button.style.opacity = 0.75

      // Apply all CSS properties from the config file
      Object.keys(element.style).forEach(styleProperty => {
        button.style[styleProperty] = element.style[styleProperty]
      })

      // Add event listener for 'mouseover' event
      button.addEventListener('mouseover', function() {
        button.style.opacity = 1.00
      });

      // Add event listener for 'mouseout' event
      button.addEventListener('mouseout', function() {
        button.style.opacity = 0.75
      });

      // Add event listener for 'mousedown' event
      button.addEventListener('mousedown', function() {
        // Scale down the button when clicked
        button.style.transform = 'scale(0.975)';
        button.style.opacity = 0.95
      });

      // Add event listener for 'mouseup' event
      button.addEventListener('mouseup', function() {
        // Scale the button back to its original size when the mouse button is released
        button.style.transform = 'scale(1)';
        button.style.opacity = 1.00
      });

      // Add event listener for 'mouseout' event
      button.addEventListener('mouseout', function() {
        // Scale the button back to its original size when the mouse leaves the button
        button.style.transform = 'scale(1)';
        button.style.opacity = 0.75
      });

      // Use the actionType field to determine which event to attach
      if (element.signal.actionType === 'onClick') {
        button.onclick = async () => {
          console.log("clicked")
          for (let callback of element.signal.callbacks) {
            // If action is a fetch call
            if (callback.type === 'FETCH') {
              let url = callback.opts.url

              // Delay execution if delay is specified
              if (callback.opts.delay) {
                console.log(callback.opts.delay)
                await new Promise(resolve => setTimeout(resolve, callback.opts.delay))
              }
              exec(`curl --user admin:admin --digest "${url}"`, (error, stdout, stderr) => {
                if (error) {
                  console.error(`exec error: ${error}`);
                  return;
                }
                console.log(`stdout: ${stdout}`);
                console.error(`stderr: ${stderr}`);
              });
            }
            // Handle other types of actions here
          }
        }
      }
      // Add more action types as needed

      app.appendChild(button)
    }

    // Add more element types as needed
  })
})

